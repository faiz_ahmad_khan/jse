package com.service;

import java.io.FileNotFoundException;

import com.exception.PositiveNumberException;

public class Calculator {

	public int div(int num1, int num2) throws PositiveNumberException {// can add any number of exception using a
																		// separator comma
		int ans = 0;
		if (num1 > 0 && num2 > 0) {
			ans = num1 / num2;
		} else {
			throw new PositiveNumberException("Please enter valid data."); // raise an exceptionally --> Intentionally
		}
		return ans;
	}
}
