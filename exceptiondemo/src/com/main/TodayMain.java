package com.main;

import java.io.FileNotFoundException;

import com.exception.PositiveNumberException;
import com.service.Calculator;

public class TodayMain {

	public static void main(String[] args) {
		Calculator calculator = null;
		try {
			calculator = new Calculator(); // dangling object are those which were not referred.
			int result = calculator.div(10, 0);
			if (result == 0) {
				System.out.println("Something is not right here!");
			} else {
				System.out.println("Division result : " + result);
			}
		} catch (PositiveNumberException e) {
			System.err.println(e.getMessage());
			// e.printStackTrace();
		} finally {
			System.out.println("Finally block"); // --> To release the memory objects
			// whether success or failure, we will always de referenced it
			calculator = null;
		}

		System.out.println("End of application");
		
		calculator = null;

	}

}
