package com.main;

import com.service.Calculator;

public class CalculatorMain {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		try {
			int var1 = Integer.parseInt(args[0]); // Converted String to int using a wrapper class
			int var2 = Integer.parseInt(args[1]);

			int ans = calculator.div(var1, var2); // CLI
			System.out.println("answer : " + ans);
		} /*
			 * catch (java.lang.ArithmeticException |
			 * java.lang.ArrayIndexOutOfBoundsException e) {//Exceptions Pipeline Example}
			 */
		catch (java.lang.ArithmeticException e) {
			// solution for the exception
			System.err.println("Solution" + e.getMessage());
		} catch (java.lang.ArrayIndexOutOfBoundsException aiobe) {
			System.err.println("Please enter the arguments" + aiobe.getMessage());
		} catch (java.lang.NumberFormatException nfe) {
			System.err.println("Please enter the correct arguments" + nfe.getMessage());
		} catch (java.lang.Exception exc) {
			System.err.println("Global Exception" + exc.getMessage());
		}
		System.out.println("The End");
		
		calculator = null;

	}

}
