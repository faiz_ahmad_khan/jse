package com.main;

public class StringBufferDemo {

	public static void main(String[] args) {
		String var1 = "Hello";
		System.out.println("StringBuffer - length " + var1.length());
		
		StringBuffer var2 = new StringBuffer();//at a time --> only one user can access it.
		var2.append("Hello");
		System.out.println("StringBuffer - length " + var2.length());
        System.out.println("StringBuffer - capacity " + var2.capacity());
		// buffer space == we can use
        var2.append("world");
        System.out.println("After append : " + var2);
        System.out.println("after append StringBuffer - length " + var2.length());
        System.out.println("after append StringBuffer - capacity " + var2.capacity());
        
        var2.append("1234567"); //17 * 2 --> (old_capacity * twice)
        System.out.println("After append123 : " + var2);
        System.out.println("after append StringBuffer - length " + var2.length());
        System.out.println("after append StringBuffer - capacity " + var2.capacity());
        System.out.println(var2.reverse());
        
        StringBuilder var3 = new StringBuilder("Hello");
        
        System.out.println("StringBuilder " + var3); //16 + 5 --> previous space + extra space
        System.out.println("after append StringBuilder - length " + var3.length());
        System.out.println("after append StringBuilder - capacity " + var3.capacity());
        
        var2 = null;
        var3 = null;
	}

}
